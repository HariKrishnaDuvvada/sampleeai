package dhk.api;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.*;

@Path("/hello")
public class HelloRest {
 
    @GET
    @Produces(MediaType.TEXT_PLAIN)
    //@Path("/{message}")
    public String publishMessage(){
         
        String responseStr = "Received message: ";
        return responseStr;
    }
}
